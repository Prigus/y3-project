import pygame
import time
import random
import sys
from pygame.locals import *
from images import *


# 2 - Initialize the game
pygame.init()
width, height = 1600, 975
screen=pygame.display.set_mode((width, height))
pygame.display.set_caption("LUDO V0.1 by Kane Easby")
# light shade of the button
color_light = (170, 170, 170)

# dark shade of the button
color_dark = (100, 100, 100)

# 4 - keep looping through

player = 2;


def roll_die():
    screen.blit(d1, (1150, 100))
    pygame.display.flip()
    time.sleep(0.1)
    screen.blit(d2, (1150, 100))
    pygame.display.flip()
    time.sleep(0.1)
    screen.blit(d3, (1150, 100))
    pygame.display.flip()
    time.sleep(0.1)
    screen.blit(d4, (1150, 100))
    pygame.display.flip()
    time.sleep(0.1)
    screen.blit(d5, (1150, 100))
    pygame.display.flip()
    time.sleep(0.1)
    screen.blit(d6, (1150, 100))
    pygame.display.flip()
    time.sleep(0.1)
    screen.blit(d7, (1150, 100))
    pygame.display.flip()
    time.sleep(0.1)
    screen.blit(d8, (1150, 100))
    pygame.display.flip()
    time.sleep(0.1)
    screen.blit(d9, (1150, 100))
    pygame.display.flip()
    time.sleep(0.1)
    screen.blit(d10, (1150, 100))
    pygame.display.flip()
    time.sleep(0.1)
    screen.blit(d11, (1150, 100))
    pygame.display.flip()
    time.sleep(0.1)
    screen.blit(d12, (1150, 100))
    pygame.display.flip()
    time.sleep(0.1)
    screen.blit(d13, (1150, 100))
    pygame.display.flip()
    time.sleep(0.1)
    die = [1,2,3,4,5,6]
    roll = random.choice(die)
    if roll == 1:
        screen.blit(one, (1150, 100))
    if roll == 2:
        screen.blit(two, (1150, 100))
    if roll == 3:
        screen.blit(three, (1150, 100))
    if roll == 4:
        screen.blit(four, (1150, 100))
    if roll == 5:
        screen.blit(five, (1150, 100))
    if roll == 6:
        screen.blit(six, (1150, 100))
    pygame.display.flip()
    return roll


y_array = [(177, 681), (115, 746), (239, 746), (177, 810)]
r_array = [(757, 103), (695, 168), (819, 168), (757, 232), (530, 70), (530, 135), (530, 200), (530, 265), (530, 330), (595, 393), (660, 393), (725, 393), (790, 393), (852, 393), (915, 393), (915, 457), (915, 522), (852, 522), (790, 522), (725, 522), (660, 522), (595, 522), (530, 586), (530, 650), (530, 714), (530, 778), (530, 842), (530, 906), (466, 906), (402, 906), (402, 842), (402, 778), (402, 714), (402, 650), (402, 586), (338, 522), (274, 522), (210, 522), (146, 522), (82, 522), (18, 522), (18, 458), (18, 394), (82, 394), (146, 394), (210, 394), (274, 394), (338, 394), (402, 330), (402, 266), (402, 202), (402, 138), (402, 74), (402, 10), (466, 10), (466, 74), (466, 138), (466, 202), (466, 266), (466, 330), (1100, 100), (1100, 180), (1100, 260), (1100, 340)]
g_array = [(177, 103), (115, 168), (239, 168), (177, 232), (82, 394), (146, 394), (210, 394), (274, 394), (338, 394), (402, 330), (402, 266), (402, 202), (402, 138), (402, 74), (402, 10), (466, 10), (530, 10), (530, 70), (530, 135), (530, 200), (530, 265), (530, 330), (595, 393), (660, 393), (725, 393), (790, 393), (852, 393), (915, 393), (915, 457), (915, 522), (852, 522), (790, 522), (725, 522), (660, 522), (595, 522), (530, 586), (530, 650), (530, 714), (530, 778), (530, 842), (530, 906), (466, 906), (402, 906), (402, 842), (402, 778), (402, 714), (402, 650), (402, 586), (338, 522), (274, 522), (210, 522), (146, 522), (82, 522), (18, 522), (18, 458), (82, 458), (146, 458), (210, 458), (274, 458), (338, 458), (1500, 100), (1500, 180), (1500, 260), (1500, 340)]
b_array = [(757, 681), (695, 746), (819, 746), (757, 810)]

# players counter
red = [4, 1, 2, 3]
green = [18, 9, 8, 7]

def position_players(red, green):
    # green
    screen.blit(g1, g_array[green[0]])
    screen.blit(g2, g_array[green[1]])
    screen.blit(g3, g_array[green[2]])
    screen.blit(g4, g_array[green[3]])

    # red
    screen.blit(r1, r_array[red[0]])
    screen.blit(r2, r_array[red[1]])
    screen.blit(r3, r_array[red[2]])
    screen.blit(r4, r_array[red[3]])

    # yellow
    screen.blit(y1, y_array[0])
    screen.blit(y2, y_array[1])
    screen.blit(y3, y_array[2])
    screen.blit(y4, y_array[3])

    # blue

    screen.blit(b1, b_array[0])
    screen.blit(b2, b_array[1])
    screen.blit(b3, b_array[2])
    screen.blit(b4, b_array[3])



color = (255,255,255)
smallfont = pygame.font.SysFont('Corbel',40)
text = smallfont.render('Roll' , True , color)



roll_value = 0


def check_catch(counter, roll_value):
    for i in range(0, 4):
        if counter < red[i] + 13 <= counter + roll_value:
            red[i] = i
            return True

def check_catch_ai (counter, roll_value):
    for i in range(0, 4):
        counter += 13
        if counter > 54:
            counter -= 54
        if counter + roll_value > 54:
            counter -= 54

        if counter < green[i] <= counter + roll_value:
            green[i] = i

def dumb_ai():
    roll_value = roll_die()
    time.sleep(1)
    can_do = True

    for i in range(0, 4):
        if red[i] > 3:
            if check_catch(red[i], roll_value):
                return 0

    for i in range(0, 4):
        if green[i] < 4 and roll_value == 6:
            for j in range(0, 4):
                if green[j] == 4:
                    can_do = False
            if can_do:
                green[i] = 4
                return 0

    for i in range(0, 4):
        if green[i] > 3 and green[i] < 60:
            green[i] += roll_value
            if green[i] > 59:
                green[i] = 60 + i
            return 0
