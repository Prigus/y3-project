from functions import *
import time
tree = []

class Node:

    def __init__(self, value, type, parent):
        self.value = value
        self.type = type
        self.parent = parent


# Initializing Nodes to None
def newNode(v, type, parent):
    temp = Node(v, type, parent)
    return temp


    # Getting expectimax
def expectiminimax(Lred):
    ai_roll = 0
    ai_move = 0
    p1_roll = 0
    p1_move = 0
    p2_roll = 0
    p2_move = 0
    def analyse_tree(parent):
        nonlocal p1_roll
        nonlocal p1_move
        nonlocal p2_roll
        nonlocal p2_move
        nonlocal ai_roll
        nonlocal ai_move
        # root =========================================================================================================
        if parent is None:
            return analyse_tree(0)
        # layer 1 max ==================================================================================================
        if parent == 0:
            highest = 0
            for i in range(1, 5):
                p1_move = i
                tree[i].value = analyse_tree(i)
                if tree[i].value > tree[highest].value:
                    highest = i
                elif highest == 0:
                    highest = i
            return tree[highest].value
        # layer 2 expecti ==============================================================================================
        if parent in range(1, 5):
            value = 0
            for i in range(5, 29):
                if tree[i].parent == parent:
                    p1_roll = tree[i].value
                    tree[i].value = analyse_tree(i)
                    value += tree[i].value
            return (value/6)
        # layer 3 min ==================================================================================================
        if parent in range(5, 29):
            ai_move = 0
            lowest = 0
            for i in range(29, 125):
                if tree[i].parent == parent:
                    ai_move += 1
                    tree[i].value = analyse_tree(i)
                    if tree[i].value < tree[lowest].value:
                        lowest = i
                    elif lowest == 0:
                        lowest = i
            return tree[lowest].value
        # layer 4 expecti ==============================================================================================
        if parent in range(29, 125):
            value = 0
            for i in range(125, 701):
                if tree[i].parent == parent:
                    ai_roll = tree[i].value
                    tree[i].value = analyse_tree(i)
                    value += tree[i].value
            return (value/6)
        # layer 5 min ==================================================================================================
        if parent in range(125, 701):
            highest = 0
            p2_move = 0
            for i in range(701, 3005):
                if tree[i].parent == parent:
                    p2_move += 1
                    tree[i].value = analyse_tree(i)
                    if tree[i].value > tree[highest].value:
                        highest = i
                    elif highest == 0:
                        highest = i
            return tree[highest].value
        # layer 6 expecti ==============================================================================================
        if parent in range(701, 3005):
            value = 0
            for i in range(3005, 16829):
                if tree[i].parent == parent:
                    p2_roll = tree[i].value
                    tree[i].value = give_score()
                    value += tree[i].value
            return (value/6)


    def give_score():
        score = 0
        temp_red = Lred.copy()
        temp_green = green.copy()
        nonlocal p1_roll
        nonlocal p2_roll
        nonlocal ai_roll
        # check if player cant move ====================================================================================
        if p1_roll is None:
            p1_roll = 0
        if p2_roll is None:
            p2_roll = 0
        if ai_roll is None:
            ai_roll = 0
        # p1 move ======================================================================================================
        check = 1
        if temp_green[p1_move - 1] > 3:
            for i in range(0, 4):
                if temp_green[p1_move - 1] < temp_red[i] + 13 < (temp_green[p1_move - 1] + p1_roll):
                    temp_red[i] = i
                    check = 0
                    score -= 20
            if check == 1:
                temp_green[p1_move - 1] += p1_roll
        elif temp_green[p1_move - 1] < 3 and p1_roll == 6:
            temp_green[p1_move - 1] = 4
        # ai move ======================================================================================================

        if temp_red[ai_move - 1] > 3:
            for i in range(0, 4):
                if temp_red[ai_move - 1] + 13 < temp_green[i] < (temp_red[ai_move - 1] + ai_roll + 13):
                    temp_green[i] = i
                    score += 100
            temp_red[ai_move - 1] += ai_roll
        elif temp_red[ai_move - 1] < 3 and ai_roll == 6:
            score += 150
            temp_red[ai_move - 1] = 4
        # p2 move ======================================================================================================
        check = 1
        if temp_green[p2_move - 1] > 3:
            for i in range(0, 4):
                if temp_green[p2_move - 1] < temp_red[i] + 13 < (temp_green[p2_move - 1] + p2_roll):
                    temp_red[i] = i
                    check = 0
                    score -= 20
            if check == 1:
                temp_green[p2_move - 1] += p2_roll
        elif temp_green[p2_move - 1] < 3 and p2_roll == 6:
            temp_green[p2_move - 1] = 4
        # score player =================================================================================================
        for i in range(0, 4):
            if temp_red[i] > 60:
                score = 0
            else:
                score += ((temp_red[i]) * 10) - ((red[i]) * 10)

        return score
    return analyse_tree(None)



def tree_builder():
    # Start Tree =======================================================================================================
    tree.append(newNode(0, 1, None))
    count = 1
    parent = 0
    # opponent turn 1 expecti ==========================================================================================
    for r in green:
        if r > 3:
            tree.append(newNode(0, 0, parent))
        else:
            tree.append(newNode(None, 0, parent))
        count += 1
    parent += 1
    # opponent rolls ===================================================================================================
    temp_count = count
    parent_position = temp_count - 4
    while parent_position < temp_count:
        if tree[parent_position].value is not None:
            for i in range(1, 7):
                tree.append(newNode(i, -1, parent_position))
                count += 1
        else:
            for i in range(1, 6):
                tree.append(newNode(None, 1, parent_position))
                count += 1
            tree.append(newNode(6, 1, parent_position))
            count += 1
        parent_position += 1
        parent += 1

    # AI turn 2 Expecti ================================================================================================
    temp_count = count
    parent_position = temp_count - 24
    while parent_position < temp_count:
        if tree[parent_position].value is not None:
            gcount = 0
            for i in range(1, 5):
                if red[gcount] > 3 or tree[parent_position].value == 6:
                    tree.append(newNode(i, 0, parent_position))
                else:
                    tree.append(newNode(None, 1, parent_position))
                gcount += 1
                count += 1
        else:
            for i in range(1, 5):
                tree.append(newNode(None, 1, parent_position))
                count += 1
        parent_position += 1
        parent += 1
    # AI Rolls  ========================================================================================================
    temp_count = count
    parent_position = temp_count - 96
    while parent_position < temp_count:
        if tree[parent_position].value is not None:
            for i in range(1, 7):
                tree.append(newNode(i, 1, parent_position))
                count += 1
        else:
            for i in range(1, 6):
                tree.append(newNode(None, 1, parent_position))
                count += 1
            tree.append(newNode(6, 1, parent_position))
            count += 1
        parent_position += 1
        parent += 1
    # Opponent turn 2 expecti ==========================================================================================
    temp_count = count
    parent_position = temp_count - 576
    while parent_position < temp_count:
        if tree[parent_position].value is not None:
            rcount = 0
            for i in range(1, 5):
                if green[rcount] > 3 or tree[parent_position].value == 6:
                    tree.append(newNode(i, 0, parent_position))
                else:
                    tree.append(newNode(None, 1, parent_position))
                rcount += 1
                count += 1
        else:
            for i in range(1, 5):
                tree.append(newNode(None, 0, parent_position))
                count += 1
        parent_position += 1
        parent += 1
    # opponent rolls ===================================================================================================
    temp_count = count
    parent_position = temp_count - 2304
    while parent_position < temp_count:
        if tree[parent_position].value is not None:
            for i in range(1, 7):
                tree.append(newNode(i, 1, parent_position))
                count += 1
        else:
            for i in range(1, 6):
                tree.append(newNode(None, 1, parent_position))
                count += 1
            tree.append(newNode(6, 1, parent_position))
            count += 1
        parent_position += 1
        parent += 1
