# 1 - Import library
import pygame
import time
import random
import sys
from functions import *
from images import *
from ai import *
from pygame.locals import *

color = (255,255,255)
smallfont = pygame.font.SysFont('Corbel',40)
largefont = pygame.font.SysFont('Corbel',120)
roll = smallfont.render('Roll' , True , color)
pone = smallfont.render('1' , True , color)
ptwo = smallfont.render('2' , True , color)
pthree = smallfont.render('3' , True , color)
pfour = smallfont.render('4' , True , color)
win = largefont.render('YOU WIN!!!' , True , color)
lose = largefont.render('YOU LOSE!!!' , True , color)
ai_vs_ai = False

def roll_click():
    if 1150 <= mouse[0] <= 1470 and 350 <= mouse[1] <= 430:
        return roll_die()
def quit():
    if ev.type == pygame.QUIT:
        # if it is quit the game
        pygame.quit()
        exit(0)

turn = 0
while 1:

    turn +=1
    if turn > 1 and player == 2:
        player = 1
    elif turn > 1 and player == 1:
        player = 2
    screen.fill((112, 128, 144))
    screen.blit(board, (-25, -25))
    pygame.draw.rect(screen, (0, 0, 0), [1150, 100, 320, 320])
    position_players(red,green)
    mouse = pygame.mouse.get_pos()
    for ev in pygame.event.get():
        # check if the event is the X button
        if ev.type == pygame.QUIT:
            # if it is quit the game
            pygame.quit()
            exit(0)
    # check if player wins
    if green[0] >= 60 and green[1] >= 60 and green[2] >= 60 and green[3] >= 60:
        screen.fill((112, 128, 144))
        screen.blit(win, (550, 450))
        pygame.display.flip()
        while 1:
            for ev in pygame.event.get():
                quit()

    if player is 1:
        if ai_vs_ai:
            dumb_ai()
        else:
            # ============================================================================================
            pause = 1
            # traps the game so it doesnt keep refreshing and restarting the turn
            while pause is 1 and turn > 1:
                # gets the mouse position and checks if it is on the roll button
                mouse = pygame.mouse.get_pos()
                for ev in pygame.event.get():
                    if 1150 <= mouse[0] <= 1470 and 350 <= mouse[1] <= 430:
                        # if the mouse is on the button change the button colour
                        pygame.draw.rect(screen, color_light, [1150, 350, 320, 80])
                        screen.blit(roll, (1285, 375))
                        pygame.display.flip()
                        # if the player presses the button roll the die
                        if ev.type == pygame.MOUSEBUTTONDOWN:
                            roll_value = roll_click()
                            pause = 0

            # if the player is not hovering over the button print the standard button
                    else:
                        pygame.draw.rect(screen, color_dark, [1150, 350, 320, 80])
                        screen.blit(roll, (1285, 375))
                        pygame.display.flip()
                    quit()
            # ============================================================================================
            pause = 1
            # traps the game so it doesnt keep refreshing and restarting the turn
            while pause is 1 and turn > 1:
                mouse = pygame.mouse.get_pos()
                for ev in pygame.event.get():
                    if green[0] < 4 and green[1] < 4 and green[2] < 4 and green[3] < 4 and roll_value < 6:
                        pause = 0
                        continue
            # green 1 ====================================================================================
                    if (roll_value is 6 and green[1] is not 4 and green[2] is not 4 and green[3] is not 4) \
                    or (green[0] >= 4 and green[1] != (green[0]+roll_value) and green[2] != (green[0]+roll_value) \
                    and green[3] != (green[0]+roll_value)) and green[0] < 60:
                        if 1080 <= mouse[0] <= 1280 and 500 <= mouse[1] <= 620:
                            # if the mouse is on the button change the button colour
                            pygame.draw.rect(screen, color_light, [1080, 500, 200, 120])
                            screen.blit(pone, (1150, 550))
                            pygame.display.flip()
                            # if the player presses the button roll the die
                            if ev.type == pygame.MOUSEBUTTONDOWN:
                                check_catch(green[0], roll_value)
                                if green[0] < 4:
                                    green[0] = 4
                                    pause = 0
                                    continue
                                else:
                                    if green[0] + roll_value > 60:
                                        green[0] = 60
                                    else:
                                        green[0] += roll_value
                                    pause = 0
                                    continue
                        else:
                            pygame.draw.rect(screen, color_dark, [1080, 500, 200, 120])
                            screen.blit(pone, (1150, 550))
                            pygame.display.flip()
            # green 2 ===================================================================================
                    if (roll_value is 6 and green[0] is not 4 and green[2] is not 4 and green[3] is not 4) \
                    or (green[1] >= 4 and green[0] != (green[1]+roll_value) and green[2] != (green[1]+roll_value) \
                    and green[3] != (green[1]+roll_value)) and green[1] < 60:
                        if 1330 <= mouse[0] <= 1530 and 500 <= mouse[1] <= 620:
                            # if the mouse is on the button change the button colour
                            pygame.draw.rect(screen, color_light, [1330, 500, 200, 120])
                            screen.blit(ptwo, (1400, 550))
                            pygame.display.flip()
                            # if the player presses the button roll the die
                            if ev.type == pygame.MOUSEBUTTONDOWN:
                                check_catch(green[1], roll_value)
                                if green[1] < 4:
                                    green[1] = 4
                                    pause = 0
                                    continue
                                else:
                                    if green[1] + roll_value > 60:
                                        green[1] = 61
                                    else:
                                        green[1] += roll_value
                                    pause = 0
                                    continue
                        else:
                            pygame.draw.rect(screen, color_dark, [1330, 500, 200, 120])
                            screen.blit(ptwo, (1400, 550))
                            pygame.display.flip()
            # green 3 ===================================================================================
                    if (roll_value is 6 and green[0] is not 4 and green[1] is not 4 and green[3] is not 4) \
                    or (green[2] >= 4 and green[0] != (green[2]+roll_value) and green[1] != (green[2]+roll_value) \
                    and green[3] != (green[2]+roll_value)) and green[2] < 60:
                        if 1080 <= mouse[0] <= 1280 and 670 <= mouse[1] <= 790:
                            # if the mouse is on the button change the button colour
                            pygame.draw.rect(screen, color_light, [1080, 670, 200, 120])
                            screen.blit(pthree, (1150, 720))
                            pygame.display.flip()
                            # if the player presses the button roll the die
                            if ev.type == pygame.MOUSEBUTTONDOWN:
                                check_catch(green[2], roll_value)
                                if green[2] < 4:
                                    green[2] = 4
                                    pause = 0
                                    continue
                                else:
                                    if green[2] + roll_value > 60:
                                        green[2] = 62
                                    else:
                                        green[2] += roll_value
                                    pause = 0
                                    continue
                        else:
                            pygame.draw.rect(screen, color_dark, [1080, 670, 200, 120])
                            screen.blit(pthree, (1150, 720))
                            pygame.display.flip()
            # green 4 ===================================================================================
                    if (roll_value is 6 and green[0] is not 4 and green[1] is not 4 and green[2] is not 4) \
                    or (green[3] >= 4 and green[0] != (green[3]+roll_value) and green[1] != (green[3]+roll_value) \
                    and green[2] != (green[3]+roll_value)) and green[3] < 60:
                        if 1330 <= mouse[0] <= 1530 and 670 <= mouse[1] <= 790:
                            # if the mouse is on the button change the button colour
                            pygame.draw.rect(screen, color_light, [1330, 670, 200, 120])
                            screen.blit(pfour, (1400, 720))
                            pygame.display.flip()
                            # if the player presses the button roll the die
                            if ev.type == pygame.MOUSEBUTTONDOWN:
                                check_catch(green[3], roll_value)
                                if green[3] < 4:
                                    green[3] = 4
                                    pause = 0
                                    continue
                                else:
                                    if green[3] + roll_value > 60:
                                        green[3] = 63
                                    else:
                                        green[3] += roll_value
                                    pause = 0
                                    continue
                    # if the player is not hovering over the button print the standard button
                        else:
                            pygame.draw.rect(screen, color_dark, [1330, 670, 200, 120])
                            screen.blit(pfour, (1400, 720))
                            pygame.display.flip()

    screen.fill((112, 128, 144))
    screen.blit(board, (-25, -25))
    pygame.draw.rect(screen, (0, 0, 0), [1150, 100, 320, 320])
    position_players(red, green)

    if turn > 1 and player == 2:
        if player == 2:
            options = []
            roll_value = roll_die()
            time.sleep(1)
            if red[0] < 4 and red[1] < 4 and red[2] < 4 and red[3] < 4 and roll_value != 6:
                continue
            tree_builder()
            for i in range(0, 4):
                score_add = 0
                AItemp = red.copy()
                if red[i] < 4 and roll_value == 6:
                    AItemp[i] = 4
                    score_add = 100
                else:
                    AItemp[i] += roll_value
                if red[i] < 4 and roll_value != 6:
                    options.append([i, score_add])
                elif red[i] >= 60:
                    options.append([i, -100000])
                else:
                    options.append([i, (expectiminimax(AItemp) + score_add)])
                if red[i] < 4 and roll_value != 6:
                    options[i][1] = 0
                if red[i] >= 60:
                    options[i][1] = -100000
                for j in red:
                    if red[i] < 4 and j == 4:
                        options[i][1] = 0
                    if red[i] + roll_value == j:
                        options[i][1] = -100000
            # move 1 ===================================================================================================
            highest = 0
            print(options)
            for i in options:
                if i[1] > options[highest][1]:
                    highest = i[0]

            # highest 1 ================================================================================================
            print(highest)
            if roll_value == 6 and red[highest] < 4:
                red[highest] = 4
                continue
            if red[highest] > 3:
                check_catch_ai(red[highest], roll_value)
                red[highest] += roll_value
                if red[highest] > 60:
                    red[options[highest][0]] = 60 + options[highest][0]
                continue
            else:
                options[highest][1] = 0
                for i in options:
                    if i[1] > options[highest][1]:
                        highest = i[0]
                # highest 2 ============================================================================================
                print(highest)
                if roll_value == 6 and red[highest] < 4:
                    red[highest] = 4
                    continue
                if red[highest] > 3:
                    check_catch_ai(red[highest], roll_value)
                    red[highest] += roll_value
                    if red[highest] > 60:
                        red[options[highest][0]] = 60 + options[highest][0]
                    continue
                else:
                    options[highest][1] = 0
                    for i in options:
                        if i[1] > options[highest][1]:
                            highest = i[0]
                    # highest 3 ========================================================================================
                    print(highest)
                    if roll_value == 6 and red[highest] < 4:
                        red[highest] = 4
                        continue
                    if red[highest] > 3:
                        check_catch_ai(red[highest], roll_value)
                        red[highest] += roll_value
                        if red[highest] > 60:
                            red[options[highest][0]] = 60 + options[highest][0]
                        continue
                    else:
                        options[highest][1] = 0
                        for i in options:
                            if i[1] > options[highest][1]:
                                highest = i[0]
                        # highest 4 ====================================================================================
                        print(highest)
                        if roll_value == 6 and red[highest] < 4:
                            red[highest] = 4
                            continue
                        if red[highest] > 3 and red[highest] < 60:
                            check_catch_ai(red[highest], roll_value)
                            red[highest] += roll_value
                            if red[highest] > 60:
                                red[options[highest][0]] = 60 + options[highest][0]
                            continue
                        else:
                            continue
    if red[0] >= 60 and red[1] >= 60 and red[2] >= 60 and red[3] >= 60:
        screen.fill((112, 128, 144))
        screen.blit(lose, (550, 450))
        pygame.display.flip()
        while 1:
            for ev in pygame.event.get():
                quit()
