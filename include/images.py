import pygame
import time
import random
import sys
from pygame.locals import *


# 3 - Load game board
board = pygame.image.load("./images/board.png")

# 3.1 - load and roll die
d1 = pygame.image.load("./images/Die/1.gif")
d2 = pygame.image.load("./images/Die/2.gif")
d3 = pygame.image.load("./images/Die/3.gif")
d4 = pygame.image.load("./images/Die/4.gif")
d5 = pygame.image.load("./images/Die/5.gif")
d6 = pygame.image.load("./images/Die/6.gif")
d7 = pygame.image.load("./images/Die/7.gif")
d8 = pygame.image.load("./images/Die/8.gif")
d9 = pygame.image.load("./images/Die/9.gif")
d10 = pygame.image.load("./images/Die/10.gif")
d11 = pygame.image.load("./images/Die/11.gif")
d12 = pygame.image.load("./images/Die/12.gif")
d13 = pygame.image.load("./images/Die/13.gif")
one = pygame.image.load("./images/Die/img1.gif")
two= pygame.image.load("./images/Die/img2.gif")
three = pygame.image.load("./images/Die/img3.gif")
four = pygame.image.load("./images/Die/img4.gif")
five = pygame.image.load("./images/Die/img5.gif")
six = pygame.image.load("./images/Die/img6.gif")

# 3.2 - load game pieces
# yellow
y1 = pygame.image.load("./images/pieces/yellow1.png")
y2 = pygame.image.load("./images/pieces/yellow2.png")
y3 = pygame.image.load("./images/pieces/yellow3.png")
y4 = pygame.image.load("./images/pieces/yellow4.png")
y1 = pygame.transform.scale(y1, (40, 60))
y2 = pygame.transform.scale(y2, (40, 60))
y3 = pygame.transform.scale(y3, (40, 60))
y4 = pygame.transform.scale(y4, (40, 60))

# blue
b1 = pygame.image.load("./images/pieces/blue1.png")
b2 = pygame.image.load("./images/pieces/blue2.png")
b3 = pygame.image.load("./images/pieces/blue3.png")
b4 = pygame.image.load("./images/pieces/blue4.png")
b1 = pygame.transform.scale(b1, (40, 60))
b2 = pygame.transform.scale(b2, (40, 60))
b3 = pygame.transform.scale(b3, (40, 60))
b4 = pygame.transform.scale(b4, (40, 60))

# red
r1 = pygame.image.load("./images/pieces/red1.png")
r2 = pygame.image.load("./images/pieces/red2.png")
r3 = pygame.image.load("./images/pieces/red3.png")
r4 = pygame.image.load("./images/pieces/red4.png")
r1 = pygame.transform.scale(r1, (40, 60))
r2 = pygame.transform.scale(r2, (40, 60))
r3 = pygame.transform.scale(r3, (40, 60))
r4 = pygame.transform.scale(r4, (40, 60))

# green
g1 = pygame.image.load("./images/pieces/green1.png")
g2 = pygame.image.load("./images/pieces/green2.png")
g3 = pygame.image.load("./images/pieces/green3.png")
g4 = pygame.image.load("./images/pieces/green4.png")
g1 = pygame.transform.scale(g1, (40, 60))
g2 = pygame.transform.scale(g2, (40, 60))
g3 = pygame.transform.scale(g3, (40, 60))
g4 = pygame.transform.scale(g4, (40, 60))